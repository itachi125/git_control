# API token for validate
API_PRIVATE_TOKEN = 'w3LH8Q9icDugzY6qFkyf'

# Filter IP requests
IP_WHITELIST = '45.127.252.170,61.28.252.90,61.28.252.91,45.127.254.227,45.127.254.229,45.127.255.164,10.0.75.1,10.0.75.2,103.196.236.3,103.196.236.5'

# Gitlab - git-so.mto.zing.vn (user: gt)
GIT_API_URL = 'https://git-so.mto.zing.vn/api/v4'
GIT_PRIVATE_TOKEN = 'AsZeV4HUoT-kDcFnUNpx'

# Path to repo
REPO_DIRECTORY = '/opt/gitcontrol/rpm'

# Logging
LOGGING_ACCESS_FILE = '/var/log/gitcontrol/access.log'
LOGGING_ERROR_FILE = '/var/log/gitcontrol/error.log'
LOGGING_ROTATE_DAY = 7
LOGGING_LEVEL = 'DEBUG'

# Path to template ansible repo gitlab
GIT_IMPORT_FILE = '/opt/gitcontrol/templates/git_automation/gt_automation_export.tar.gz'
GIT_CLONE_DIR = '/opt/gitcontrol/templates/git_automation'
GIT_CLONE_URL = 'https://gt:TlJ0kOWgQqcnV5naPkET@git-so.mto.zing.vn/gt'