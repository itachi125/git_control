#!/usr/bin/env python
# -*- coding: utf-8 -*-

from app import flask_app

if __name__ == '__main__':
    flask_app.run(host="172.19.0.13", port=7070, debug=False)
