#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Nang Huynh'

import requests
from pathlib import PurePath
from app import flask_app
from functools import wraps
import random
import string
import os

from app.modules.logging import make_logger
app_logger = make_logger(name='app.modules.gitlab_executor')


def error_handler(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except requests.RequestException as err:
            app_logger.error('{}'.format(err), exc_info=True)
            pass
        except Exception as err:
            app_logger.error('{}'.format(err), exc_info=True)
            pass
    return wrapper

class GitlabExecutor(object):
    def __init__(self):
        self.api_url = '{}'.format(flask_app.config['GIT_API_URL'])
        self.private_token = '{}'.format(flask_app.config['GIT_PRIVATE_TOKEN'])
        self.header = {
            'Private-Token': self.private_token
        }

    @error_handler
    def check_project_exists(self, name):
        s = requests.Session()
        req = s.get(url='{}/projects?search={}&simple=true&owned=true'.format(self.api_url, name), headers=self.header)
        if req and req.status_code == 200 and len(req.json()) > 0:
            project_info = req.json()
            for project in project_info:
                if project['name'].lower() == name.lower():
                    return True
        return False

    @error_handler
    def get_project_by_name(self, name):
        s = requests.Session()
        req = s.get(url='{}/projects?search={}&simple=true&owned=true'.format(self.api_url, name), headers=self.header)
        if req and req.status_code == 200 and len(req.json()) > 0:
            project_info = req.json()
            app_logger.debug(project_info)
            for project in project_info:
                if project['name'].lower() == name.lower():
                    return {'id': project['id'], 'http_url': project['http_url_to_repo'], 'ssh_url': project['ssh_url_to_repo'], 'web_url': project['web_url']}

    @error_handler
    def check_user(self, name):
        s = requests.Session()
        req = s.get(url='{}/users?username={}'.format(self.api_url, name), headers=self.header)
        if req and req.status_code == 200 and len(req.json()) > 0:
                return True
        return False

    @error_handler
    def create_user(self, username, password):
        app_logger.info('Create new git username: {}'.format(username))
        if password is None:
            password = '{}'.format(''.join(random.choices(string.ascii_letters + string.digits, k=12)))
            app_logger.info('new_password: {}'.format(password))
        payload = {
            'email': '{}@vng.com.vn'.format(username),
            'password': '{}'.format(password),
            'username': '{}'.format(username),
            'name': '{}'.format(username),
            'skip_confirmation': 'true',
            'reset_password': 'false',
            'provider': 'cas3',
            'extern_uid': '{}'.format(username)
        }
        app_logger.debug(payload)

        s = requests.Session()
        req = s.post(url='{}/users'.format(self.api_url), data=payload, headers=self.header)
        if req and req.status_code == 201 and len(req.json()) > 0:
            user_info = req.json()
            app_logger.debug(user_info)
            # update email
            _s = requests.Session()
            _payload = {
                'email': '{}@vng.com.vn'.format(username)
            }
            _req = _s.put(url='{}/users/{}'.format(self.api_url, user_info['id']), data=_payload, headers=self.header)
            if _req and _req.status_code == 200:
                app_logger.info('updated email success')
            else:
                app_logger.error('updated email fail')
            return user_info['id']

    @error_handler
    def git_clone_and_update_template(self, project_name):
        app_logger.info('Clone template to {} project'.format(project_name))
        path_git = '{}'.format(flask_app.config['GIT_CLONE_DIR'])
        # url = '{}'.format(flask_app.config['GIT_CLONE_URL'])
        path_project = path_git + '/{}'.format(project_name)
        url_template = '{}/automation.git'.format(flask_app.config['GIT_CLONE_URL'])
        url_project = '{}/{}.git'.format(flask_app.config['GIT_CLONE_URL'], project_name)
        app_logger.debug(url_template)
        app_logger.debug(url_project)
        os.system('export HOME=\"/home/gitcontrol\"; git config --global user.email \"gt.git.so@vng.com.vn\"; git config --global user.name \"gt\"')
        os.system('export HOME=\"/home/gitcontrol\"; cd ' + path_git + '; git clone ' + url_template)
        os.system('export HOME=\"/home/gitcontrol\"; cd ' + path_git + '; git clone ' + url_project)
        os.system('rsync -ar ' + path_git + '/automation/* ' + path_project)
        os.system('export HOME=\"/home/gitcontrol\"; cd ' + path_project + '; git add *')
        os.system('export HOME=\"/home/gitcontrol\"; cd ' + path_project + '; git commit -m \"add new\"')
        os.system('export HOME=\"/home/gitcontrol\"; cd ' + path_project + '; git push ' + url_project + ' master')
        os.system('cd ' + path_git + '; rm -rf automation; rm -rf {}'.format(project_name))
        # os.system('/bin/bash {}'.format(path_git) + '/git_clone.sh {} {} {} >> /var/log/gitcontrol/git_clone.log 2>&1'.format(project_name, path_git, url))
        app_logger.debug("Done")


    @error_handler
    def create_project(self, name):
        app_logger.info('Create the git project {}'.format(name))
        payload = {
            'name': '{}'.format(name.lower()),
            'description': 'Using for {} project'.format(name),
            'visibility': 'private',
            'lfs_enabled': 'true'
        }
        app_logger.debug(payload)

        if self.check_project_exists(name=name):
            return self.get_project_by_name(name=name)
        s = requests.Session()
        req = s.post(url='{}/projects'.format(self.api_url), data=payload, headers=self.header)
        app_logger.debug(req.status_code)
        if req and req.status_code == 201 and len(req.json()) > 0:
            project_info = req.json()
            app_logger.debug(project_info)
            self.git_clone_and_update_template(project_name=name)
            if project_info:
                return {'id': project_info['id'], 'http_url': project_info['http_url_to_repo'], 'ssh_url': project_info['ssh_url_to_repo'], 'web_url': project_info['web_url']}

    @error_handler
    def get_user_id_by_name(self, name):
        s = requests.Session()
        req = s.get(url='{}/users?username={}'.format(self.api_url, name), headers=self.header)
        if req and req.status_code == 200 and len(req.json()) > 0:
            user_info = req.json()
            for user in user_info:
                if user['username'] == name:
                    return user['id']

    @error_handler
    def get_project_id_by_name(self, name):
        s = requests.Session()
        req = s.get(url='{}/projects?search={}&simple=true&owned=true'.format(self.api_url, name), headers=self.header)
        if req and req.status_code == 200 and len(req.json()) > 0:
            project_info = req.json()
            for project in project_info:
                if project['name'].lower() == name.lower():
                    return project['id']

    @error_handler
    def get_files_by_suffix(self, name, suffix='.yml', match='playbook_'):
        project_id = self.get_project_id_by_name(name=name)
        if project_id:
            s = requests.Session()
            # / projects /: id / repository / tree
            # match use for match substring, gitlab will respond all files that they have suffix and match param
            req = s.get(url='{}/projects/{}/repository/tree?recursive=true'.format(self.api_url, project_id), headers=self.header)
            if req and req.status_code == 200 and len(req.json()) > 0:
                project_data = req.json()
                app_logger.debug(project_data)
                return [data.get('path') for data in project_data if data['type'] == 'blob' and PurePath(data.get('path')).suffix == suffix and PurePath(data.get('path')).match('deploy/{}*'.format(match))]

    @error_handler
    def add_user_to_project(self, user_id, project_id):
        app_logger.info('Add user_id: {} to project_id: {}'.format(user_id, project_id))
        """
        Valid access levels:
            10 => Guest access
            20 => Reporter access
            30 => Developer access
            40 => Maintainer access
            50 => Owner access # Only valid for groups
        """
        payload = {
            'user_id': user_id,
            'access_level': 40
        }
        app_logger.debug(payload)
        # POST /projects/:id/members
        s = requests.Session()
        req = s.post(url='{}/projects/{}/members'.format(self.api_url, project_id), data=payload, headers=self.header)
        app_logger.debug(req.status_code)
        if req and req.status_code == 201:
            if len(req.json()) > 0:
                app_logger.info(req.json())
            return True
        return False

    @error_handler
    def remove_user_from_project(self, user_id, project_id):
        app_logger.info('Remove user_id: {} from project_id: {}'.format(user_id, project_id))
        # DELETE /projects/:id/members/:user_id
        s = requests.Session()
        req = s.delete(url='{}/projects/{}/members/{}'.format(self.api_url, project_id, user_id), headers=self.header)
        app_logger.debug(req.status_code)
        if req and req.status_code == 204:
            if req.status_code != 204:
                app_logger.info(req.json())
            return True
        return False

### END ###
