#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
import logging
import logging.config
from logging.handlers import TimedRotatingFileHandler
from app import flask_app

def has_level_handler(logger):
    level = logger.getEffectiveLevel()
    current = logger

    while current:
        if any(handler.level <= level for handler in current.handlers):
            return True
        if not current.propagate:
            break
        current = current.parent

    return False

def make_logger(name):
    logger = logging.getLogger(name)

    access_file = flask_app.config['LOGGING_ACCESS_FILE']
    error_file = flask_app.config['LOGGING_ERROR_FILE']
    level = flask_app.config['LOGGING_LEVEL']
    backup_count = int(flask_app.config['LOGGING_ROTATE_DAY'])
    # log format
    formatter = logging.Formatter('[%(asctime)s] [%(name)s] [%(funcName)s:%(lineno)s] [%(levelname)s] %(message)s')

    logger.setLevel(eval('logging.{}'.format(level)))
    logger.propagate = True

    if access_file is not None and error_file is not None:
        access_handler = TimedRotatingFileHandler(filename=access_file, when='midnight', interval=1, backupCount=backup_count)
        access_handler.setLevel(logging.DEBUG)
        access_handler.setFormatter(formatter)

        error_handler = TimedRotatingFileHandler(filename=error_file, when='midnight', interval=1, backupCount=backup_count)
        error_handler.setLevel(logging.ERROR)
        error_handler.setFormatter(formatter)

        if not has_level_handler(logger):
            logger.addHandler(access_handler)
            logger.addHandler(error_handler)

    return logger

""" END """