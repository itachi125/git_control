#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Blueprint
from flask import request
from flask import jsonify
from app import flask_app
from werkzeug.exceptions import HTTPException
import random
import string
import glob
import os

from app.modules.gitlab_executor import GitlabExecutor

from app.modules.logging import make_logger
app_logger = make_logger(name='app.views')

# Define the blueprint
api = Blueprint('api', __name__)

@api.before_app_request
def validate_request():
    app_logger.debug(request.remote_addr)
    ip_whitelist = flask_app.config['IP_WHITELIST']
    if ip_whitelist:
        ip_whitelist = ip_whitelist.split(',')
        app_logger.debug(ip_whitelist)
        if not str(request.remote_addr) in ip_whitelist:
            return jsonify({
                'response_code': -9,
                'response_message': 'Permission denied',
                'response_data': {}
            })

    app_logger.debug('{}'.format(request.headers))
    if request.headers.get('Content-Type') != 'application/json':
        return jsonify({
            'response_code': -111,
            'response_message': 'Invalid header: Content-Type',
            'response_data': {}
        })
    else:
        if not request.is_json:
            return jsonify({
                'response_code': -110,
                'response_message': 'Invalid format: json required',
                'response_data': {}
            })
    if request.headers.get('Private-Token', '') != '{}'.format(flask_app.config['API_PRIVATE_TOKEN']):
        return jsonify({
            'response_code': -111,
            'response_message': 'Invalid header: Private-Token',
            'response_data': {}
        })

@api.errorhandler(400)
def bad_request(err):
    app_logger.error('{}'.format(err), exc_info=True)
    return jsonify({
        'response_code': -400,
        'response_message': 'Bad Request',
        'response_data': {}
    }), 400

@api.errorhandler(404)
def page_not_found(err):
    app_logger.error('{}'.format(err), exc_info=True)
    return jsonify({
        'response_code': -404,
        'response_message': 'The requested URL was not found',
        'response_data': {}
    }), 404

@api.errorhandler(405)
def method_not_allowed(err):
    app_logger.error('{}'.format(err), exc_info=True)
    return jsonify({
        'response_code': -200,
        'response_message': 'The requested Method is not allowed',
        'response_data': {}
    }), 405

@api.errorhandler(500)
def internal_server_error(err):
    app_logger.error('{}'.format(err), exc_info=True)
    return jsonify({
        'response_code': -11,
        'response_message': 'Internal Server Error',
        'response_data': {}
    }), 500

@api.errorhandler(HTTPException)
def handle_api_error(err):
    app_logger.error('{}'.format(err), exc_info=True)
    return jsonify({
        'response_code': -11,
        'response_message': 'Internal Server Error',
        'response_data': {}
    }), 500


# /api/v1/projects/<project_name>/files?suffix=yml?match=playbook_
@api.route('/projects/<string:project_name>/files', methods=['GET'])
def get_files(project_name):
    suffix = request.args.get('suffix', None)
    match = request.args.get('match', None)
    if suffix:
        app_logger.info('suffix: .{}'.format(suffix))
    if match:
        app_logger.info('match: .{}'.format(match))
    gitlab_executor = GitlabExecutor()
    lst_files = gitlab_executor.get_files_by_suffix(name=project_name, suffix='.{}'.format(suffix) if suffix is not None else '.yml', match='{}*'.format(match) if match is not None else 'playbook_')
    return jsonify({
        'response_code': 0,
        'response_message': 'OK',
        'response_data': {
            'playbook_yml': lst_files
        }
    })

@api.route('/projects', methods=['POST'])
def create_project():
    req_data = request.get_json(force=True)
    project_name = req_data.get('project_name')
    if project_name is not None:
        gitlab_executor = GitlabExecutor()
        project_info = gitlab_executor.create_project(name=project_name)
        if project_info:
            return jsonify({
                'response_code': 0,
                'response_message': 'Success',
                'response_data': project_info,
            })
        else:
            return jsonify({
                'response_code': -1,
                'response_message': 'Fail',
                'response_data': {},
            })
    return jsonify({
        'response_code': -1,
        'response_message': 'Fail',
        'response_data': {
            'detail': 'project_name not exists in POST data'
        }
    })

"""
/api/v1/projects/<project_name>/users
payload = {
    'username': nanght
}
"""
@api.route('/projects/<string:project_name>/users', methods=['POST'])
def add_user_to_project(project_name):
    req_data = request.get_json(force=True)
    username = req_data.get('username')
    if username is not None:
        gitlab_executor = GitlabExecutor()
        if not gitlab_executor.check_user(name=username):
            password = '{}'.format(''.join(random.choices(string.ascii_letters + string.digits, k=12)))
            user_id = gitlab_executor.create_user(username=username, password=password)
        else:
            user_id = gitlab_executor.get_user_id_by_name(name=username)
        project_id = gitlab_executor.get_project_id_by_name(name=project_name)
        if gitlab_executor.add_user_to_project(user_id=user_id, project_id=project_id):
            return jsonify({
                'response_code': 0,
                'response_message': 'Success',
                'response_data': {},
            })
        else:
            return jsonify({
                'response_code': -1,
                'response_message': 'Fail',
                'response_data': {},
            })
    return jsonify({
        'response_code': -1,
        'response_message': 'Fail',
        'response_data': {
            'detail': 'username not exists in POST data'
        }
    })

"""
/api/v1/projects/<project_name>/users
payload = {
    'username': nanght
}
"""
@api.route('/projects/<string:project_name>/users', methods=['DELETE'])
def remove_user_from_project(project_name):
    req_data = request.get_json(force=True)
    username = req_data.get('username')
    if username is not None:
        gitlab_executor = GitlabExecutor()
        if not gitlab_executor.check_user(name=username):
            password = '{}'.format(''.join(random.choices(string.ascii_letters + string.digits, k=12)))
            user_id = gitlab_executor.create_user(username=username, password=password)
        else:
            user_id = gitlab_executor.get_user_id_by_name(name=username)
        project_id = gitlab_executor.get_project_id_by_name(name=project_name)
        if gitlab_executor.remove_user_from_project(user_id=user_id, project_id=project_id):
            return jsonify({
                'response_code': 0,
                'response_message': 'Success',
                'response_data': {},
            })
        else:
            return jsonify({
                'response_code': -1,
                'response_message': 'Fail',
                'response_data': {},
            })
    return jsonify({
        'response_code': -1,
        'response_message': 'Fail',
        'response_data': {
            'detail': 'username not exists in POST data'
        }
    })

@api.route('/files/rpm', methods=['GET'])
def list_rpm_files():
    rpm_files = [os.path.basename(f) for f in glob.glob('{}/*.rpm'.format(flask_app.config['REPO_DIRECTORY']))]
    app_logger.info(rpm_files)
    return jsonify({
        'response_code': 0,
        'response_message': 'RPM',
        'response_data': {
            'rpm': rpm_files
        }
    })
