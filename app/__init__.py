#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
# Import Flask libs
from flask import Flask
from flask import jsonify
from werkzeug.exceptions import HTTPException

flask_app = Flask(__name__, instance_relative_config=True)
# Load the default configuration
flask_app.config.from_object('config.default')
flask_app.config.from_object('config.{}'.format(flask_app.config['ENV']))

# Logging
from app.modules.logging import make_logger
app_logger = make_logger(name='app.main')

from app.views import api as api_module
flask_app.register_blueprint(api_module, url_prefix='/api/v1')

@flask_app.errorhandler(400)
def bad_request(err):
    app_logger.error('{}'.format(err))
    return jsonify({
        'response_code': -400,
        'response_message': 'Bad Request',
        'response_data': {}
    }), 400

@flask_app.errorhandler(404)
def page_not_found(err):
    app_logger.error('{}'.format(err))
    return jsonify({
        'response_code': -404,
        'response_message': 'The requested URL was not found',
        'response_data':{}
    }), 404

@flask_app.errorhandler(405)
def method_not_allowed(err):
    app_logger.error('{}'.format(err))
    return jsonify({
        'response_code': -200,
        'response_message': 'The requested Method is not allowed',
        'response_data': {}
    }), 405

@flask_app.errorhandler(500)
def internal_server_error(err):
    app_logger.error('{}'.format(err))
    return jsonify({
        'response_code': -11,
        'response_message': 'Internal Server Error',
        'response_data': {}
    }), 500

@flask_app.errorhandler(HTTPException)
def handle_api_error(err):
    app_logger.error('{}'.format(err))
    return jsonify({
        'response_code': -11,
        'response_message': 'Internal Server Error',
        'response_data': {}
    }), 500
